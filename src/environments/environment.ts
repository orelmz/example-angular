// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDcqN-OyBz-b0sazDwjvrFddvrzfvvMjsk",
    authDomain: "example-orelmz.firebaseapp.com",
    projectId: "example-orelmz",
    storageBucket: "example-orelmz.appspot.com",
    messagingSenderId: "269315039846",
    appId: "1:269315039846:web:0d425044a5bd599f0543a7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
